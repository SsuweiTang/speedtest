package cs.ntust.edu.speedtest.parameter;

/**
 * Created by wei on 2016/10/14.
 */

public class NetworkConnectDescriptor {
    public static final int NETWORKTYPE_INVALID = 0;
    public static final int NETWORKTYPE_WAP = 1;
    public static final int NETWORKTYPE_2G = 2;
    public static final int NETWORKTYPE_3G = 3;
    public static final int NETWORKTYPE_WIFI = 4;
}
