package cs.ntust.edu.speedtest.domain;


/**
 * @author Ssu-Wei,Tang
 */
public class SpeedResult {
    String networkType;
    String testTime;
    String uploadSpeed;
    String downloadSpeed;
    String totalTime;

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public void setUploadSpeed(String uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getNetworkType() {
        return networkType;
    }

    public String getTestTime() {
        return testTime;
    }

    public String getUploadSpeed() {
        return uploadSpeed;
    }

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public String getTotalTime() {
        return totalTime;
    }


}
