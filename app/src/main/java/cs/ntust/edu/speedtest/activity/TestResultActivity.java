package cs.ntust.edu.speedtest.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import cs.ntust.edu.speedtest.R;

/**
 * @author Ssu-Wei,Tang
 */
public class TestResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
    }
}
