package cs.ntust.edu.speedtest.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import cs.ntust.edu.speedtest.activity.TestResultActivity;
import cs.ntust.edu.speedtest.util.NetworkConnectHandler;

/**
 * Created by wei on 2016/10/14.
 */


public class TestSpeedTask extends AsyncTask<Void, Integer, String> {

    long start;
    long end;
    long cost;
    int s = 0;
    private Context mContext;

    public TestSpeedTask(Context context) {
        start = System.currentTimeMillis();

        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected String doInBackground(Void... arg0) {

        s = NetworkConnectHandler.getNetWorkType(mContext);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        jumpToResult();

        end = System.currentTimeMillis();
        cost = (end - start);
        Log.v("cost", String.valueOf(cost) + "s" + s);


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();


    }

    private void jumpToResult() {
        Intent intent = new Intent();
        intent.setClass(mContext, TestResultActivity.class);
        mContext.startActivity(intent);
        ((Activity) mContext).finish();
    }
}

