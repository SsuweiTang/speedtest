package cs.ntust.edu.speedtest.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import cs.ntust.edu.speedtest.R;
import cs.ntust.edu.speedtest.task.TestSpeedTask;

/**
 * @author Ssu-Wei,Tang
 */
public class SpeedTestActivity extends AppCompatActivity {
    private Button buttonTestNetworkSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_test);
        findView();
    }

    private void findView() {
        buttonTestNetworkSpeed = (Button) findViewById(R.id.buttonTestNetworkSpeed);
        buttonTestNetworkSpeed.setOnClickListener(SpeedTestClickListener);

    }

    private Button.OnClickListener SpeedTestClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            new TestSpeedTask(SpeedTestActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }


    };
}